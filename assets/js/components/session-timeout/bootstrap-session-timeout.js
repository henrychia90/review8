! function(a) {
    "use strict";
    a.sessionTimeout = function(b) {
        function c() {
            n || (a.ajax({
                type: i.ajaxType,
                url: i.keepAliveUrl,
                data: i.ajaxData
            }), n = !0, setTimeout(function() {
                n = !1
            }, i.keepAliveInterval))
        }

        function d() {
            clearTimeout(g), (i.countdownBar) && f("session", !0), "function" == typeof i.onStart && i.onStart(i), i.keepAlive && c(), g = setTimeout(function() {
                "function" != typeof i.onWarn(i), e()
            }, i.warnAfter)
        }

        function f(b, c) {
            clearTimeout(j.timer), "dialog" === b && c ? j.timeLeft = Math.floor((i.redirAfter - i.warnAfter) / 1e3) : "session" === b && c && (j.timeLeft = Math.floor(i.redirAfter / 1e3)), i.countdownBar && "dialog" === b ? j.percentLeft = Math.floor(j.timeLeft / ((i.redirAfter - i.warnAfter) / 1e3) * 100) : i.countdownBar && "session" === b && (j.percentLeft = Math.floor(j.timeLeft / (i.redirAfter / 1e3) * 100));
            var d = a(".countdown-holder"),
                e = j.timeLeft >= 0 ? j.timeLeft : 0;
            if (i.countdownSmart) {
                var g = Math.floor(e / 60),
                    h = e % 60,
                    k = g > 0 ? g + "m" : "";
                k.length > 0 && (k += " "), k += h + "s", d.text(k)
            } else d.text(e + "s");
            i.countdownBar && a(".countdown-bar").css("width", j.percentLeft + "%"), j.timeLeft = j.timeLeft - 1, j.timer = setTimeout(function() {
                f(b)
            }, 1e3)
        }
        var g, h = {
                onStart: !1,
                onWarn: !1,
                onRedir: !1,
                countdownBar: !1,
                countdownSmart: !1
            },
            i = h,
            j = {};
        if (b && (i = a.extend(h, b)), i.warnAfter >= i.redirAfter) return console.error('Bootstrap-session-timeout plugin is miss-configured. Option "redirAfter" must be equal or greater than "warnAfter".'), !1;
        var n = !1;
        d()
    }
}(jQuery);

var SessionTimeout=function() {
    var e=function() {
        $.sessionTimeout( {
            warnAfter:6e3, 
            redirAfter:21e3, 
            ignoreUserActivity:!0, 
            countdownMessage:"Redirecting in {timer}.", 
            countdownBar: !0
        }
        )
    };
    return {
        init:function() {
            e()
        }
    }
}

();
jQuery(document).ready(function() {
    SessionTimeout.init()
}
);
